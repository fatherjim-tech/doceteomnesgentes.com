---
title: "Rev. James Smith"
photo: "images/teams/frjim.jpg"
email: "email2@example.org"
bg_image: "images/feature-bg.jpg"
photo: "/images/teams/frjim.jpg"
draft: false
social:
  - icon : "ion-monitor" #ionicon pack v2 : https://ionicons.com/v2/
    link : "https://fatherjim.tech"
  - icon : "ion-play" #ionicon pack v2 : https://ionicons.com/v2/
    link : "https://www.youtube.com/channel/UC3lhp0Xsr22O-RFjcBQZJOA"
#  - icon : "ion-social-pinterest-outline" #ionicon pack v2 : https://ionicons.com/v2/
#    link : "#"
---

Fr. Jim is a native Trentonian and priest of the Diocese of Trenton, New Jersey. After completing his seminary formation at St. Mary’s Seminary and University in Baltimore, MD, Fr. Jim was ordained to the priesthood of Jesus Christ on June 2, 2018.
